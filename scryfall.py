import json
import sys
import os
import urllib.request

def collector_key(x):
    return (x + '0' if x[-1].isdigit() else x).rjust(10, '0')


url = 'https://api.scryfall.com/bulk-data/default-cards'

print(f'load descriptor file from {url}...')
with urllib.request.urlopen(url) as response:
    if response.getcode() == 200:
        bulk = json.load(response)
        url = bulk['download_uri']
    else:
        print('failed to download descriptor!')
        sys.exit(1)


print(f'load card data from {url}...')
with urllib.request.urlopen(url) as response:
    if response.getcode() == 200:
        scryfall = json.load(response)
    else:
        print('failed to download card data!')
        sys.exit(1)


print('loading sets...')
codes = []
with open('public/sets.json', 'r') as f:
    for c, s in json.load(f).items():
        for v in s['codes']:
            codes.append(v)


print('creating card dictionary...')
sets = {}
for card in scryfall:
    code = card['set']
    if code in codes and not card['digital']:
        s = sets.get(code, {'code': code, 'name': card['set_name'], 'count_booster': 0, 'count_base': 0, 'count_all': 0, 'cards': {}})

        if 'image_uris' in card:
            im = [card['image_uris']['normal']]
        elif 'card_faces' in card and 'image_uris' in card['card_faces'][0] and 'image_uris' in card['card_faces'][1]:
            im = [card['card_faces'][0]['image_uris']['normal'], card['card_faces'][1]['image_uris']['normal']]
        else:
            im = []

        c = {'id': card['id'], 'cm': card.get('cardmarket_id', None), 'name': card['name'], 'l': card['layout'] + '/' + card['border_color'], 'r': card['rarity'], 'b': card['booster'], 'p': card['promo'], 'f': card.get('finishes', []), 'fe': card.get('frame_effects', []), 'pt': card.get('promo_types', []), 'pr': card.get('prices', {}), 'im': im}
        s['count_all'] = s['count_all'] + 1
        if c['b']:
            s['count_booster'] = s['count_booster'] + 1
        if len(c['pt']) == 0:
            s['count_base'] = s['count_base'] + 1
        s['cards'][card['collector_number']] = c
        sets[code] = s


print('writing set files...')
for code, set in sets.items():
    keys = sorted([*set['cards']], key = collector_key)
    cards = {k:set['cards'][k] for k in keys}
    set['cards'] = cards
    with open(f'public/cards_{code}.json', 'w') as f:
        json.dump(set, f, indent = 2)
